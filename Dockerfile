FROM jupyter/minimal-notebook:latest



RUN conda update -n base conda

COPY requirements.txt /tmp/
RUN pip install --requirement /tmp/requirements.txt

RUN fix-permissions $CONDA_DIR && \
    fix-permissions /home/$NB_USER

RUN conda install -y jupyter_contrib_nbextensions 

WORKDIR $HOME/work